;;----------------------------------------------------------------------------------;;
;;		Boot Code for configuring the stripe prior to 			    ;;
;;			running any meaningful program				    ;;
;;----------------------------------------------------------------------------------;;

;;----------------------------------------------------------------------------------;;
;; Written by: Krishna Sekar
;; Date	     : February 2002
;; All rights reserved
;;----------------------------------------------------------------------------------;;

;;----------------------------------------------------------------------------------;;
;;
;;  This code will set the chip's memory map and other register settings
;;  from their default values. 
;;  It also initializes the stack pointers and interrupts for each mode, and 
;;  finally branches to __main in the C library (which eventually calls main()).
;;
;;  On reset, the ARM core starts up in Supervisor (SVC) mode, in ARM state,
;;  with IRQ and FIQ disabled.
;;
;;----------------------------------------------------------------------------------;;

	
	AREA Boot_Code,CODE,READONLY
	
	ENTRY

;;----------------------------------------------------------------------------------;;
;; This region contains the exception vectors for exception processing
;;----------------------------------------------------------------------------------;;

Exception_Vectors

    	b   Start			; Reset
    	b   Unexpected			; Undefined Instructions
    	b   Unexpected			; Software Interrupt
    	b   Unexpected			; Prefetch Abort
    	b   Unexpected			; Data Abort
    	b   Unexpected			; Unused
    	b   Unexpected			; IRQ
	b   Unexpected			; FIQ

;;----------------------------------------------------------------------------------;;
;; Unnexpected exception handler. Have not yet implemented exception handlers
;; for these types of exceptions
;;----------------------------------------------------------------------------------;;

Unexpected 	
	b   Unexpected			; Keep looping

;;----------------------------------------------------------------------------------;;
;; Start of configuration program
;;----------------------------------------------------------------------------------;;

Start
   
;;----------------------------------------------------------------------------------;;
;; Set up the memory map
;; 
;; Registers	- 0x7FFFC000 - 0x7FFFFFFF
;; EBI0		- 0x00000000 - 0x001FFFFF	; The FLASH is present here
;; EBI1,2,3	- Not used (default values)
;; SDRAM0,1	- Not used (default values) 
;; SRAM0 	- 0x40000000 - 0x4001FFFF 	; SRAM0 and SRAM1 form a contiguous
;; SRAM1	- 0x40020000 - 0x4003FFFF	; memory region
;; DPSRAM0 	- 0x60000000 - 0x6000FFFF
;; DPSRAM1 	- 0x70000000 - 0x7000FFFF
;; PLD0		- Not used (default values)
;; PLD1,2,3	- Not used (default values)
;;
;;----------------------------------------------------------------------------------;;

;; The register base addr @ reset is 7FFFC000
;; The value of MMAP_REGISTERS at reset is 7FFFC683
;; We will not change the default memory map of the register region

;; Setup SRAMS and DPSRAMS

	ldr r0, =MMAP_SRAM0_ADDR
	ldr r1, =0x40000801
	str r1, [r0]

	ldr r0, =MMAP_SRAM1_ADDR
	ldr r1, =0x40020801
	str r1, [r0]
	
	ldr r0, =MMAP_DPSRAM0_ADDR
	ldr r1, =0x60000781
	str r1, [r0]
	
	ldr r0, =MMAP_DPSRAM1_ADDR
	ldr r1, =0x70000781
	str r1, [r0]

;; Setup EBI0 i.e., the FLASH memory

	ldr r0, =MMAP_EBI0_ADDR
	ldr r1, =0x00000A01
	str r1, [r0]
	
;; Rest of the MMAP registers have their default values for now

;;----------------------------------------------------------------------------------;;
;;
;; Set up the Clocks (PLLs) 
;; Here we are setting only PLL1 which will clock AHB1 and AHB2
;; AHB1 freq set to 100MHz ( hence AHB2 freq is 50 MHz ) (ARM PSP timing shell gives
;; warnings at more than 133Mhz)
;; PLL2 is disabled since not yet using SDRAM
;;
;;----------------------------------------------------------------------------------;;

	ldr r0, =CLK_PLL1_NCNT_ADDR
	ldr r1, =0x00020101		; N = 2
	str r1, [r0]

	ldr r0, =CLK_PLL1_MCNT_ADDR
	ldr r1, =0x00020404		; M = 8
	str r1, [r0]

	ldr r0, =CLK_PLL1_KCNT_ADDR
	ldr r1, =0x00050100		; K = 1
	str r1, [r0]

	ldr r0, =CLK_PLL1_CTRL_ADDR
	ldr r1, =0x00001A05		; PLL1 enabled
	str r1, [r0]

	ldr r0, =CLK_PLL2_CTRL_ADDR
	ldr r1, =0x00001A04		; PLL2 disabled
	str r1, [r0]

	ldr r0, =CLK_DERIVE_ADDR
	ldr r1, =0x00002010 		; Bypass PLL2 only
	str r1, [r0]

;;----------------------------------------------------------------------------------;;
;; All other registers have their default values for now
;; Will change later depending upon requirement
;;----------------------------------------------------------------------------------;;

;;----------------------------------------------------------------------------------;;
;; Initializing the stack pointers now 
;; The stack is a full descending stack
;; The base of the stack is at 0x4003FFFC
;; I've put the SVC stack on top, followed by IRQ and USR stacks
;;----------------------------------------------------------------------------------;;

;; -- Entering SVC mode and setting up the SVC stack pointer --

	msr CPSR_c, #MODE_SVC:OR:I_BIT:OR:F_BIT ; setting SVC mode, disabling IRQ, FIQ
	ldr sp, =SVC_STACK_ADDR 

;; -- Entering IRQ mode and setting up the IRQ stack pointer --

	msr CPSR_c, #MODE_IRQ:OR:I_BIT:OR:F_BIT ; setting IRQ mode, disabling IRQ, FIQ
	ldr sp, =IRQ_STACK_ADDR 

;; -- Setup other stack pointers if necessary

;;----------------------------------------------------------------------------------;;
;; Initialize the memory management system, caches etc 
;;----------------------------------------------------------------------------------;;

;; -- Talk to Shoubhik about this

;;----------------------------------------------------------------------------------;;
;; Initialize whatever else needs to be initialized
;;----------------------------------------------------------------------------------;;

;; -- Initialize critical IO devices

;; -- Initialize Interrupt System variables

;;----------------------------------------------------------------------------------;;
;; Change to User mode and set up User mode stack
;;----------------------------------------------------------------------------------;;

;; -- Entering USR mode and setting up the USR stack pointer --

	msr CPSR_c, #MODE_USR:OR:I_BIT:OR:F_BIT ; setting USR mode, disabling IRQ, FIQ
	ldr sp, =USR_STACK_ADDR 

;;----------------------------------------------------------------------------------;;
;; Now enter the C code and start doing useful stuff
;; main() will be called ultimately from __main
;;----------------------------------------------------------------------------------;;

	IMPORT  __main

	b	__main	; use b not bl since application does not return here 

;;----------------------------------------------------------------------------------;;
;; Some EQU definitions for easier readability
;; Will put this in a separate file later
;;----------------------------------------------------------------------------------;;

REGISTER_BASE_ADDR	EQU	0x7FFFC000

MMAP_REGISTERS_ADDR	EQU	REGISTER_BASE_ADDR + 0x080
MMAP_SRAM0_ADDR		EQU	REGISTER_BASE_ADDR + 0x090
MMAP_SRAM1_ADDR		EQU	REGISTER_BASE_ADDR + 0x094
MMAP_DPSRAM0_ADDR	EQU	REGISTER_BASE_ADDR + 0x0A0
MMAP_DPSRAM1_ADDR	EQU	REGISTER_BASE_ADDR + 0x0A4
MMAP_EBI0_ADDR		EQU	REGISTER_BASE_ADDR + 0x0C0

CLK_PLL1_NCNT_ADDR	EQU	REGISTER_BASE_ADDR + 0x300
CLK_PLL1_MCNT_ADDR	EQU	REGISTER_BASE_ADDR + 0x304
CLK_PLL1_KCNT_ADDR	EQU	REGISTER_BASE_ADDR + 0x308
CLK_PLL1_CTRL_ADDR	EQU	REGISTER_BASE_ADDR + 0x30C
CLK_PLL2_CTRL_ADDR	EQU	REGISTER_BASE_ADDR + 0x31C
CLK_DERIVE_ADDR		EQU	REGISTER_BASE_ADDR + 0x320


;;----------------------------------------------------------------------------------;;
;; Standard definitions of mode bits and interrupt (I & F) flags in PSRs
;;----------------------------------------------------------------------------------;;

MODE_USR		EQU	0x10
MODE_FIQ		EQU	0x11
MODE_IRQ		EQU 	0x12
MODE_SVC		EQU	0x13
MODE_ABT		EQU	0x17
MODE_UNDEF		EQU	0x1B
MODE_SYS		EQU	0x1F

I_BIT			EQU	0x80 ; when the I bit is set, IRQ is disabled
F_BIT			EQU	0x40 ; when the F bit is set, FIQ is disabled

;;----------------------------------------------------------------------------------;;
;; System memory locations for Stack and Heap
;; I'm using the model of a contiguous memory where
;; RW code+data, ZI, stack and heap is located in the on-chips SRAMs.
;; All RO code+data is located in the FLASH.
;; Before execution, __main copies RW code+data, initializes ZI, sets up the 
;; stack and heap and ultimately calls main().
;;----------------------------------------------------------------------------------;;

SVC_STACK_ADDR		EQU	0x4003FFFC 	 	; SVC stack at top of SRAM
IRQ_STACK_ADDR		EQU	SVC_STACK_ADDR - 256	; Followed by IRQ stack
USR_STACK_ADDR		EQU	IRQ_STACK_ADDR - 256	; Followed by USR stack


	END

