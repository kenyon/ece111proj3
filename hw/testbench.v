/////////////////////////////////////////////////////////////////////////
//	This testbench contains the Altera Stripe model EPXA10
//	and the ARM920t core from Seamless along with a FLASH device
// 	for "boot from flash" mode.
//	This is for software only simulations.
//	
//	File	  : 	test_bench.v
//	Created by:	Krishna Sekar
//	Date	  :	01/22/02
//
/////////////////////////////////////////////////////////////////////////

`timescale 	1ns / 10ps

module test_bench;

`define CLK_REF_PERIOD	20 // This corresponds to a 50Mhz external clock

  reg		clk_ref;
  reg   	reset_drive;

  integer 	i;

// Reset signal

  wand 		resetn;

// EBI signals to connect to Flash memory
  
  wire  [15:0]    ebi_dq_pin;         // Data
  wire            ebi_ack_pin;        // Common acknowledge for all CS
  wire  [24:0]    ebi_a_pin;          // Address
  wire            ebi_we_n_pin;       // Write enable
  wire            ebi_oe_n_pin;       // Output enable
  wire  [1:0]     ebi_be_pin;         // Byte enables
  wire  [3:0]     ebi_cs_pin;         // Chip Selects
  wire            ebi_clk_pin;        // Clock wire



  initial 
	begin
		clk_ref	= 1'b0;
		reset_drive = 1'b0;
	end

  always
	begin
		# (`CLK_REF_PERIOD/2) clk_ref = ~clk_ref;
	end

  initial 
	begin
       		$display("\n%t******* Simulation Begins",$time,"******");

      		for(i=0;i<2;i=i+1)@(posedge clk_ref);

      		reset_drive = 1'b1;

      		for(i=0;i<10;i=i+1)@(posedge clk_ref);

       		$display("\n\t*********************************************");
       		$display("\t****   Altera Excalibur Stripe Design    ****");
       		$display("\t**** ---------------------------------   ****");
       		$display("\t****  Altera Stripe Swift Model +        ****");
       		$display("\t****  ARM920t Seamless simulation model  ****");
       		$display("\t****  				     ****");
       		$display("\t****  Reset in progress		     ****");
       		$display("\t****     				     ****");
       		$display("\t****        			     ****");
       		$display("\t****                                     ****");
       		$display("\t*********************************************");
       		$display("\t*********************************************");
       		$display("\n%t:Reset Released",$time);
       		$display("\t*********************************************");

      		reset_drive = 1'b0;

      		#50000000 $finish; // In Case of infinite loops
    	end
	

  assign resetn = ~reset_drive;



//////////////////////////////////////////////////////////////////////////
//
// Instantiate the Altera Stripe + ARM920t core model
//
//////////////////////////////////////////////////////////////////////////

alt_exc_seamless
	#("ARM",         // processor
          1000,          // device size
          32,            // sdram_width
          4,             // sdramdqm_width
          0,             // gpio_width
          "TRUE",        // boot from flash
          "TRUE",        // debug extensions - a low value enables hw watchdog
          16,            // EBI width
          "TRUE",        // Use short reset  - ?? was TRUE in initial testbench
          "FALSE",       // Don't use initialization files (boot from flash)
          "1xSPx32",	 // DP0 is configured as one block of 16K x 32
          "1xSPx32",	 // DP1 is configured as one block of 16K x 32
          32,		 // DP0 data width is 32
          32,		 // DP1 data width is 32
          16,		 // DP0 address width is 16
          16,		 // DP1 address width is 16
          "UNREG",       // DP0 output not registered
          "UNREG"        // DP1 output not registered
         )

	u_stripe (
                        .clk_ref                (clk_ref),
                        .nreset                 (resetn),
                        .npor                   (1'b1),
                        .proc_tdi               (1'b0),
                        .proc_tdo               (),
                        .proc_tms               (1'b0),
                        .proc_tck               (1'b0),
                        .proc_ntrst             (1'b1),

                        .masterhclk             (1'b0),
                        .masterhready           (1'b0),
                        .masterhgrant           (1'b0),
                        .masterhresp            (2'b0),
                        .masterhrdata           (32'b0),
                        .masterhwrite           (),
                        .masterhlock            (),
                        .masterhbusreq          (),
                        .masterhtrans           (),
                        .masterhsize            (),
                        .masterhburst           (),
                        .masterhaddr            (),
                        .masterhwdata           (),

                        .slavehclk              (1'b0),
                        .slavehwrite            (1'b0),
                        .slavehreadyi           (1'b0),
                        .slavehselreg           (1'b0),
                        .slavehsel              (1'b0),
                        .slavehmastlock         (1'b0),
                        .slavehaddr             (32'b0),
                        .slavehwdata            (32'b0),
                        .slavehtrans            (2'b0),
                        .slavehsize             (2'b0),
                        .slavehburst            (3'b0),
                        .slavehreadyo           (),
                        .slavebuserrint         (),
                        .slavehrdata            (),
                        .slavehresp             (),

                        .debugrq                (1'b0),
                        .debugext0              (1'b0),
                        .debugext1              (1'b0),
                        .debugiebrkpt           (1'b0),
                        .debugdewpt             (1'b0),
                        .debugextin             (4'b0),

                        .intpld                 (6'b0), 
                        .intuart                (),
                        .inttimer0              (),
                        .inttimer1              (),
                        .intcommtx              (),
                        .intcommrx              (),
                        .intextpin              (1'b1),

                        .debugack               (),
                        .debugrng0              (),
                        .debugrng1              (),
                        .debugextout            (),

                        .ebidq                  (ebi_dq_pin),
                        .ebiaddr                (ebi_a_pin),
                        .ebiwen                 (ebi_we_n_pin),
                        .ebioen                 (ebi_oe_n_pin),
                        .ebibe                  (),
                        .ebicsn                 (ebi_cs_pin),
                        .ebiack                 (),
                        .ebiclk                 (),

                        .uartctsn               (1'b1),
                        .uartdsrn               (1'b1),
                        .uartrxd                (1'b0),
                        .uarttxd                (),
                        .uartrtsn               (),
                        .uartdtrn               (),
                        .uartdcdn               (),
                        .uartrin                (),

                        .sdramdq                (),
                        .sdramdqs               (),
                        .sdramdqm               (),
                        .sdramaddr              (),
                        .sdramclk               (),
                        .sdramclkn              (),
                        .sdramclke              (),
                        .sdramwen               (),
                        .sdramcasn              (),
                        .sdramrasn              (),
                        .sdramcsn               (),

                        .traceclk               (),
                        .tracepipestat          (),
                        .tracepkt               (),
                        .tracesync              (),

                        .lockreqdp0             (1'b0),
                        .lockgrantdp0           (),
                        .lockreqdp1             (1'b0),
                        .lockgrantdp1           (),

                        .dp0_portaaddr          (16'b0),
                        .dp0_2_portaclk         (1'b0),
                        .dp0_portaena           (1'b0),
                        .dp0_portawe            (1'b0),
                        .dp0_portadatain        (32'b0),
                        .dp0_portadataout       (),

                        .dp0_portbaddr          (16'b0),
                        .dp0_portbclk           (1'b0),
                        .dp0_portbena           (1'b0),
                        .dp0_portbwe            (1'b0),
                        .dp0_portbdatain        (32'b0),
                        .dp0_portbdataout       (),

                        .dp1_portaaddr          (16'b0),
                        .dp1_3_portaclk         (1'b0),
                        .dp1_portaena           (1'b0),
                        .dp1_portawe            (1'b0),
                        .dp1_portadatain        (32'b0),
                        .dp1_portadataout       (),

                        .dp1_portbaddr          (16'b0),
                        .dp1_portbclk           (1'b0),
                        .dp1_portbena           (1'b0),
                        .dp1_portbwe            (1'b0),
                        .dp1_portbdatain        (32'b0),
                        .dp1_portbdataout       (),

                        .dp2_portaaddr          (16'b0),
                        .dp2_portaena           (1'b0),
                        .dp2_portawe            (1'b0),
                        .dp2_portadatain        (32'b0),
                        .dp2_portadataout       (),

                        .dp3_portaaddr          (16'b0),
                        .dp3_portaena           (1'b0),
                        .dp3_portawe            (1'b0),
                        .dp3_portadatain        (32'b0),
                        .dp3_portadataout       ()
		);
			
//////////////////////////////////////////////////////////////////////////
//
// Instantiate the Intel Flash 28f160c3b_90 16Mbit x 16 Denali Model
//
//////////////////////////////////////////////////////////////////////////

m28f160c3b_90 u_flash 
      ( .a        (ebi_a_pin[20:1]),
    	.dq       (ebi_dq_pin[15:0]),
    	.cebar    (ebi_cs_pin[0]), 	// By default CS is active low
    	.oebar    (ebi_oe_n_pin),
    	.webar    (ebi_we_n_pin),
    	.wpbar    (1'b1),
    	.rpbar    (resetn)
      );


endmodule
